#include <conio.h>
#include "nbtree_d.h"

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char** argv) {
	
	nbTree pTree;
	nbAddr srcNode;
	nbCreate(&pTree);
	
	nbInsert (&pTree, pTree.root, 1);
	nbInsert (&pTree, pTree.root, 2);
	nbInsert (&pTree, pTree.root, 3);
	nbInsert (&pTree, pTree.root, 8);
	nbInsert (&pTree, pTree.root->fs, 4);
	nbInsert (&pTree, pTree.root->fs->fs, 5);
	nbInsert (&pTree, pTree.root->fs->fs, 6);
	nbInsert (&pTree, pTree.root->fs->fs, 7);
	
	nbPrint(pTree.root,0);
	
	return 0;
}
